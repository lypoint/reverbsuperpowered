package com.asiainno.media;

/**
 * Created by lypoint on 2016/8/9 0009.
 */

public class Reverb {
    public static final int extraSample = 64;
    static {
        System.loadLibrary("reverb");
    }

    private int samplerate;
    private float dry;
    private float wet;
    private float mix;
    private float width;
    private float damp;
    private float rommSize;
    private boolean enable;
    private float[] tempBuffer;
    private float[] tempBuffer2;

    public Reverb(int samplerate) {
        this.samplerate = samplerate;
    }

    public Reverb dry(float value) {
        dry = value;
        return this;
    }

    public Reverb wet(float value) {
        wet = value;
        return this;
    }

    public Reverb mix(float value) {
        mix = value;
        return this;
    }

    public Reverb width(float value) {
        width = value;
        return this;
    }

    public Reverb damp(float value) {
        damp = value;
        return this;
    }

    public Reverb rommSize(float value) {
        rommSize = value;
        return this;
    }

    public Reverb enabl(boolean value) {
        enable = value;
        return this;
    }

    public void release() {
        reset();
    }

    public Reverb create() {
        if (samplerate != 0) {
            setSamplerate(samplerate);
        }
        if (dry != 0) {
            setDry(dry);
        }
        if (wet != 0) {
            setWet(wet);
        }
        if (mix != 0) {
            setMix(mix);
        }
        if (width != 0) {
            setWidth(width);
        }
        if (damp != 0) {
            setDamp(damp);
        }
        if (rommSize != 0) {
            setRoomSize(rommSize);
        }
        enable(enable);
        return this;
    }

    public void processData(byte[] in, byte[] out, int numberOfSamples) throws Exception {
        if (samplerate == 0) {
            throw new Exception("had you create it ?");
        }
        if (tempBuffer == null || tempBuffer.length < numberOfSamples + extraSample) {
            tempBuffer = new float[numberOfSamples + extraSample];
        }
        if (tempBuffer2 == null || tempBuffer2.length < numberOfSamples + extraSample) {
            tempBuffer2 = new float[numberOfSamples + extraSample];
        }
        processByte(in, out, tempBuffer,tempBuffer2, numberOfSamples);
    }

    private static native void setSamplerate(int value);

    private static native void setDry(float value);

    private static native void setWet(float value);

    private static native void setMix(float value);

    private static native void setWidth(float value);

    private static native void setDamp(float value);

    private static native void setRoomSize(float value);

    private static native void enable(boolean value);

    private static native void reset();

    private static native void process(float[] in, float[] out, int numberOfSamples);

    private static native void processByte(byte[] in, byte[] out, float[] temp,float[] temp2, int numberOfSamples);
}
