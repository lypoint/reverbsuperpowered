package com.asiainno.media;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {
    SeekBar dry;
    SeekBar wet;
    SeekBar mix;
    SeekBar width;
    SeekBar damp;
    SeekBar roomSize;

    ProgressBar progressBar;

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dry = (SeekBar) findViewById(R.id.seekbar1);
        wet = (SeekBar) findViewById(R.id.seekbar2);
        mix = (SeekBar) findViewById(R.id.seekbar3);
        width = (SeekBar) findViewById(R.id.seekbar4);
        damp = (SeekBar) findViewById(R.id.seekbar5);
        roomSize = (SeekBar) findViewById(R.id.seekbar6);
        progressBar = (ProgressBar) findViewById(R.id.loading);
        textView = (TextView) findViewById(R.id.text);

        dry.setProgress(0);
        wet.setProgress(0);
        mix.setProgress(0);
        width.setProgress(0);
        damp.setProgress(0);
        roomSize.setProgress(0);
        dry.setOnSeekBarChangeListener(this);
        wet.setOnSeekBarChangeListener(this);
        mix.setOnSeekBarChangeListener(this);
        width.setOnSeekBarChangeListener(this);
        damp.setOnSeekBarChangeListener(this);
        roomSize.setOnSeekBarChangeListener(this);

        Button button = (Button) findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(runnable).start();
            }
        });
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.VISIBLE);
                }
            });
            Reverb reverb = new Reverb(44100);
            float value = 0.0f;
            final StringBuffer stringBuffer = new StringBuffer();
            value = getSeekValut(dry.getProgress());
            stringBuffer.append("\ndry=" + value);
                reverb.dry(value);
            value = getSeekValut(wet.getProgress());
            stringBuffer.append("\nwet=" + value);
                reverb.wet(value);
            value = getSeekValut(mix.getProgress());
            stringBuffer.append("\nvalue=" + value);
                reverb.mix(value);
            value = getSeekValut(width.getProgress());
            stringBuffer.append("\nwidth=" + value);
                reverb.width(value);
            value = getSeekValut(damp.getProgress());
            stringBuffer.append("\ndamp=" + value);
                reverb.damp(value);
            value = getSeekValut(roomSize.getProgress());
            stringBuffer.append("\nroomSize=" + value);
                reverb.rommSize(value);
            reverb.enabl(true).create();

            System.out.println(stringBuffer.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText(stringBuffer.toString());
                }
            });

            try {
                //FileInputStream inputStream = new FileInputStream("/sdcard/44100_02.pcm");
                InputStream inputStream = getAssets().open("44100_02.pcm");
                FileOutputStream outputStream = new FileOutputStream(getSdcardPath(getApplicationContext()) + "/4100_02_out.pcm");
                int read;
                byte[] buffer = new byte[2048 + Reverb.extraSample * 2];
                byte[] buffer2 = new byte[2048 + Reverb.extraSample * 2];
                while ((read = inputStream.read(buffer, 0, buffer.length - Reverb.extraSample * 2)) > 0) {
                    reverb.processData(buffer, buffer2, read / 2);
                    outputStream.write(buffer2, 0, read);
                }
                inputStream.close();
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            reverb.release();

            try {
                FileInputStream inputStream = new FileInputStream(getSdcardPath(getApplicationContext()) + "/4100_02_out.pcm");
                byte[] buffer = new byte[10240];
                AudioTrack track = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, buffer.length, AudioTrack.MODE_STREAM);
                track.play();//开始
                int read;
                while ((read = inputStream.read(buffer, 0, buffer.length)) > 0) {
                    track.write(buffer, 0, read);
                }
                track.stop();//停止播放
                track.release();//释放底层资源。
            } catch (Exception e) {
                e.printStackTrace();
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    };

    public void onSeekChanged() {
        final StringBuffer stringBuffer = new StringBuffer();
        float value = getSeekValut(dry.getProgress());
        stringBuffer.append("\ndry=" + value);
        value = getSeekValut(wet.getProgress());
        stringBuffer.append("\nwet=" + value);
        value = getSeekValut(mix.getProgress());
        stringBuffer.append("\nvalue=" + value);
        value = getSeekValut(width.getProgress());
        stringBuffer.append("\nwidth=" + value);
        value = getSeekValut(damp.getProgress());
        stringBuffer.append("\ndamp=" + value);
        value = getSeekValut(roomSize.getProgress());
        stringBuffer.append("\nroomSize=" + value);
        textView.setText(stringBuffer.toString());
    }

    private float getSeekValut(int progress) {
        float seek = 0.5f;
        seek = progress * 1.0f / 100;
        return seek;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.exit(0);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        onSeekChanged();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public static String getSdcardPath(Context context) {
        return Environment.getExternalStorageDirectory() != null ? Environment.getExternalStorageDirectory().getAbsolutePath() : context.getCacheDir().getAbsolutePath();
    }
}
