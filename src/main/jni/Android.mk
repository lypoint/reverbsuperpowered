#ifeq ($(strip $(BUILD_WITH_GST)),true)

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)


LOCAL_SRC_FILES:= com_asiainno_media_reverb.cpp

LOCAL_LDFLAGS := libSuperpoweredAndroidARM.a

LOCAL_LDLIBS    := -llog -lz

LOCAL_ARM_MODE := arm  
  
LOCAL_MODULE:= libreverb
  
LOCAL_CFLAGS := -DHAD_CONFIG_H -DFPM_ARM -ffast-math -O3
LOCAL_CFLAGS    += -ffast-math

include $(BUILD_SHARED_LIBRARY) 

#endif
