#include "com_asiainno_media_Reverb.h"

SuperpoweredReverb *reverb;
SuperpoweredResampler *resampler;

JNIEXPORT void JNICALL Java_com_asiainno_media_Reverb_setDry(JNIEnv * env, jclass jobject, jfloat value){
    reverb->setDry((float)value);
    LOGD("reverb set,dry%f", value);
}

JNIEXPORT void JNICALL Java_com_asiainno_media_Reverb_setWet(JNIEnv * env, jclass jobject, jfloat value){
    reverb->setWet((float)value);
    LOGD("reverb set,wet%f", value);
}

JNIEXPORT void JNICALL Java_com_asiainno_media_Reverb_setMix(JNIEnv * env, jclass jobject, jfloat value){
    reverb->setMix((float)value);
    LOGD("reverb set,mix%f", value);
}


JNIEXPORT void JNICALL Java_com_asiainno_media_Reverb_setWidth(JNIEnv * env, jclass jobject, jfloat value){
    reverb->setWidth((float)value);
    LOGD("reverb set,width%f", value);
}

JNIEXPORT void JNICALL Java_com_asiainno_media_Reverb_setDamp(JNIEnv * env, jclass jobject, jfloat value){
    reverb->setDamp((float)value);
    LOGD("reverb set,damp%f", value);
}


JNIEXPORT void JNICALL Java_com_asiainno_media_Reverb_setRoomSize(JNIEnv * env, jclass jobject, jfloat value){
    reverb->setRoomSize((float)value);
    LOGD("reverb set,roomSize%f", value);
}


JNIEXPORT void JNICALL Java_com_asiainno_media_Reverb_enable (JNIEnv * env, jclass jobject, jboolean value){
    reverb->enable(value);
    LOGD("reverb set,enavle%d", value);
}


JNIEXPORT void JNICALL Java_com_asiainno_media_Reverb_setSamplerate (JNIEnv * env, jclass jobject, jint value){
    unsigned int sampleRs = (unsigned int) value;
    reverb = new SuperpoweredReverb(sampleRs);
    resampler = new SuperpoweredResampler();
//    resampler->rate = sampleRs;
    LOGD("\nreverb init ----------------------sampleRs=%d-----------------------------------", sampleRs);
}


JNIEXPORT void JNICALL Java_com_asiainno_media_Reverb_reset (JNIEnv *env, jclass jobject){
    reverb->reset();
    resampler->reset();
    LOGD("reverb reset --------------------------------------------------------------------\n");
}

JNIEXPORT void JNICALL Java_com_asiainno_media_Reverb_processByte (JNIEnv *env, jclass jobject, jbyteArray data,jbyteArray dataOut, jfloatArray dataTemp, jfloatArray dataTemp2,jint dataLen){
    jbyte *inData = env->GetByteArrayElements(data, 0);
    jbyte *outData = env->GetByteArrayElements(dataOut, 0);
    jfloat *tempData = env->GetFloatArrayElements(dataTemp, 0);
    jfloat *tempData2 = env->GetFloatArrayElements(dataTemp2, 0);

    jsize inSize = env->GetArrayLength(data);
    jsize outSize = env->GetArrayLength(dataOut);
    jsize tempSize = env->GetArrayLength(dataTemp);
    if (inSize<dataLen * 2) {
        LOGD("in data len error,realInLen=%d,inLen=%d,realOutLen=%d,", inSize, dataLen, outSize);
        return;
    }

    short *sampleIn = (short *) inData;
    short *sampleOut = (short *) outData;
    float *sampleTemp = (float *) tempData;
    float *sampleTemp2 = (float *) tempData2;

//    LOGD("resample start numberOfSamples=%d,inSamples=%d,outSamples=%d",dataLen/2,inSize/2,tempSize);
//    此处函数似不可用，出来的值都是0
//    int temLen = resampler->process(sampleIn, sampleTemp, dataLen/2, false, true, 1.0f);
//    LOGD("resample complete,inLen=%d,outLen=%d", dataLen/2, temLen);
    for(int i=0;i<dataLen;i++){
        sampleTemp[i] = (float)sampleIn[i];
//        LOGD("reverb covert i=%d in=%d temp=%f", i,sampleIn[i],sampleTemp[i]);
    }
//    LOGD("reverb1 covert to float end");

    bool isRev = reverb->process(sampleTemp, sampleTemp2, dataLen/2);
//    LOGD("reverb2 process result%d", isRev);
    for(int i=0;i<dataLen;i++){
        sampleOut[i] = (short)sampleTemp2[i];
    }
//    LOGD("reverb3 covert to short,in[0]=%d,temp[0]=%f,out[0]=%d",sampleIn[0],sampleTemp[0],sampleOut[0]);

    env->ReleaseByteArrayElements(data, inData,0);
    env->ReleaseByteArrayElements(dataOut, outData,0);
    env->ReleaseFloatArrayElements(dataTemp, tempData,0);
    env->ReleaseFloatArrayElements(dataTemp2, tempData2,0);
}


JNIEXPORT void JNICALL
Java_com_asiainno_media_Reverb_process
(JNIEnv
*env,
jclass jobject, jfloatArray
in,
jfloatArray out, jint
size
){

}